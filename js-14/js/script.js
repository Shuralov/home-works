$(document).ready(function(){
    $(".link, .upwards").click(function(){
        $('html,body').animate({ scrollTop: $($(this).attr("href")).offset().top }, 1000);
    });

    $(window).scroll(function() {
      if ($(document).scrollTop() > this.innerHeight) {
          $('.upwards').css('opacity', '100%')
      }
          else if ($(document).scrollTop() < this.innerHeight)  {
              $('.upwards').css('opacity', '0')
      }
    });

    $(".toggle").click(function(){
        $(this).text(function(i, label){
            return label === "Show" ? "Hide" : "Show";
        });
        $('.hot-news').slideToggle(500);
    });

});