let currentPrice = document.querySelector('.current-price-eyebrow-none');
let input = document.getElementById('input');
let error = document.querySelector('.error-message-none');
let spanContent = document.querySelector('.current-price-eyebrow-none');
let img = document.createElement('img');
img.src = "img/times-circle-regular.svg";

function goodPrice() {
    if (input.value>=0 && input.value!=='') {
        spanContent.innerHTML = '';
        error.classList.remove('error-message');
        error.classList.add('error-message-none');
        input.classList.add('input-good-price');
        currentPrice.classList.remove('current-price-eyebrow-none');
        currentPrice.classList.add('current-price-eyebrow');
        let text = document.createTextNode(`Current price: ${input.value}`);
        spanContent.append(text);
        spanContent.append(img);
    }
}

function badPrice() {
    if (input.value<0) {
        currentPrice.classList.remove('current-price-eyebrow');
        currentPrice.classList.add('current-price-eyebrow-none');
        error.classList.remove('error-message-none');
        error.classList.add('error-message');
    }
}

function clear () {
    input.value = '';
    input.classList.remove('input-good-price');
    currentPrice.classList.remove('current-price-eyebrow');
    currentPrice.classList.add('current-price-eyebrow-none');
}

img.addEventListener('click', clear);
input.addEventListener('blur', badPrice);
input.addEventListener('blur', goodPrice);




















