function filterBy(arr,dataType) {
    let filtered =[];
    arr.forEach(el=>{
        if (typeof(el) !== dataType) {
            filtered.push(el);
        }
    });
    return filtered;
}

console.log(filterBy([1,'word', true, undefined, null, NaN, {} ], 'number'));
console.log(filterBy([1,'word', true, undefined, null, NaN, {} ], 'string'));
console.log(filterBy([1,'word', true, undefined, null, NaN, {} ], 'boolean'));
console.log(filterBy([1,'word', true, undefined, null, NaN, {} ], 'undefined'));
console.log(filterBy([1,'word', true, undefined, null, NaN, {} ], 'object'));
console.log(filterBy([1,'word', true, undefined, null, NaN, {} ], 'object'));





