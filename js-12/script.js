let img = document.querySelectorAll("img");
let stopBtn = document.querySelector(".stop");
let resumeBtn = document.querySelector(".resume");
let timer;
let j=0;

img[img.length-1].classList.add('image-to-show');

function carousel () {
    for (let i=0;i<img.length; i++) {
        img[i].classList.remove('image-to-show');
    }
    img[j].classList.add('image-to-show');
    j++;

    if (j===img.length) {
        j=0;
    }
    stopBtn.addEventListener('click', stop);
    timer =  setTimeout(carousel, 10000);
}

function stop() {
    if (j===0) {
        j=img.length-1;
        clearTimeout(timer);
        stopBtn.removeEventListener("click", stop);
    }
else
    j--;
    clearTimeout(timer);
    stopBtn.removeEventListener("click", stop);
}


stopBtn.addEventListener('click', stop);
resumeBtn.addEventListener('click', carousel);
setTimeout(carousel, 10000);


