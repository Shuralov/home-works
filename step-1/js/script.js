const tabs = document.querySelectorAll(".services--tabs--item");
const cards = document.querySelectorAll(".services--card");
const loadMoreBtn = document.querySelector(".our-work--button");
const filterButtons = document.querySelectorAll(".our-work--list--tabs--item");
const brakingNews = document.querySelectorAll('.braking-news--list--card');
const title = document.querySelectorAll('.braking-news--list--card--title');
const date = document.querySelectorAll('.braking-news--list--card--date');

let pressed; // хранит позицию нажатой карточки Braking news (из ТЗ непонятно что означает "кликабельный" - решил вопрос так)


for (let z = 0; z < brakingNews.length; z++) {

    brakingNews[z].addEventListener('click', function () {
        pressed=z;
        title.forEach((elem) => {
            elem.style.color = "#717171";
        })
        date.forEach((elem) => {
            elem.style.backgroundColor = '#1E3631';
        })
        title[z].style.color = "#18cfab";
        date[z].style.backgroundColor = "#18cfab";
    })
}

    for (let y = 0; y < brakingNews.length; y++) {
    brakingNews[y].addEventListener('mouseover', function () {
        title.forEach((elem) => {
            elem.style.color = "#717171";
        })
        date.forEach((elem) => {
            elem.style.backgroundColor = '#1E3631';
        })
        title[y].style.color = "#18cfab";
        date[y].style.backgroundColor = "#18cfab";

        title[pressed].style.color = "#18cfab";
        date[pressed].style.backgroundColor = "#18cfab";
    })
}


for (let k = 0; k < tabs.length; k++) {

    tabs[k].addEventListener('click', function() {
        for (let i = 0; i < tabs.length; i++) {
            tabs[i].classList.remove('active');
            cards[i].classList.add("hidden");
        }
        tabs[k].classList.add("active");
        cards[k].classList.remove("hidden");
    })
}


ready ();


function ready() {
    let a = 0;
        const arr = ['graphic-design', 'web-design', 'landing-page', 'wordpress'];
        const tabsArr = ['all', 'graphic-design', 'web-design', 'landing-page', 'wordpress'];
            document.getElementById('our-work--list--cards').innerHTML = '';
            filterButtons.forEach((elem) => {
                elem.classList.remove('services--tabs--item--active');
            })
            for (let i = 1; i < 13; i++) {
                let lot = Math.floor(Math.random() * 4);
                let img = document.createElement("img");
                let overlay = document.createElement("div");
                let container = document.createElement("div");
                let iconContainer = document.createElement("div");
                let iconRef = document.createElement("div");
                let iconSearch = document.createElement("div");

                iconContainer.classList.add('icon--container');
                overlay.classList.add('after');
                container.classList.add('image-container');

                img.onerror = function () {
                    this.style.display = 'none'
                };
                img.src = `img/${tabsArr[a]}/${tabsArr[a]}${i}.jpg`

                document.getElementById('our-work--list--cards').appendChild(container);
                container.style.width = '284px'
                container.style.height = '206px'
                container.appendChild(img);
                container.appendChild(overlay);

                let icon = document.createElement("img");
                icon.src = `img/icons/reference.png`;
                let icon2 = document.createElement("img");
                icon2.src = `img/icons/small-search.png`;

                iconRef.appendChild(icon);
                iconRef.classList.add('icon--style--ref');
                iconSearch.appendChild(icon2);
                iconSearch.classList.add('icon--style--search');
                iconContainer.appendChild(iconRef);
                iconContainer.appendChild(iconSearch);
                overlay.appendChild(iconContainer);

                overlay.innerHTML += "<h5 class='reviews--person--name'>CREATIVE DESIGN</h5>";

                if (a === 0) {

                    img.src = `img/${arr[lot]}/${arr[lot]}${Math.floor((Math.random() * 6) + 1)}.jpg`
                    overlay.innerHTML += `<p class='reviews--person--position'>${(arr[lot]).replace("-", " ")}</p>`;
                } else {

                    overlay.innerHTML += `<p class='reviews--person--position'>${(tabsArr[a]).replace("-", " ")}</p>`;

                }

                filterButtons[a].classList.add('services--tabs--item--active');
            }
}




    for (let a = 0; a < filterButtons.length; a++) {

        const arr = ['graphic-design', 'web-design', 'landing-page', 'wordpress'];
        const tabsArr = ['all', 'graphic-design', 'web-design', 'landing-page', 'wordpress'];
        filterButtons[a].addEventListener('click', function ready() {

            document.getElementById('our-work--list--cards').innerHTML = '';
            filterButtons.forEach((elem) => {
                elem.classList.remove('services--tabs--item--active');
            })
            for (let i = 1; i < 13; i++) {
                let lot = Math.floor(Math.random() * 4);

                let img = document.createElement("img");
                let overlay = document.createElement("div");
                let container = document.createElement("div");
                let iconContainer = document.createElement("div");
                let iconRef = document.createElement("div");
                let iconSearch = document.createElement("div");

                container.style.width = '284px'
                container.style.height = '206px'

                iconContainer.classList.add('icon--container');
                overlay.classList.add('after');
                container.classList.add('image-container');

                img.onerror = function () {
                    this.style.display = 'none'
                };
                img.src = `img/${tabsArr[a]}/${tabsArr[a]}${i}.jpg`

                document.getElementById('our-work--list--cards').appendChild(container);
                container.appendChild(img);
                container.appendChild(overlay);

                let icon = document.createElement("img");
                icon.src = `img/icons/reference.png`;

                let icon2 = document.createElement("img");
                icon2.src = `img/icons/small-search.png`;

                iconRef.appendChild(icon);
                iconRef.classList.add('icon--style--ref');
                iconSearch.appendChild(icon2);
                iconSearch.classList.add('icon--style--search');
                iconContainer.appendChild(iconRef);
                iconContainer.appendChild(iconSearch);
                overlay.appendChild(iconContainer);

                overlay.innerHTML += "<h5 class='reviews--person--name'>CREATIVE DESIGN</h5>";

                if (a === 0) {

                    img.src = `img/${arr[lot]}/${arr[lot]}${Math.floor((Math.random() * 6) + 1)}.jpg`
                    overlay.innerHTML += `<p class='reviews--person--position'>${(arr[lot]).replace("-", " ")}</p>`;
                } else {

                    overlay.innerHTML += `<p class='reviews--person--position'>${(tabsArr[a]).replace("-", " ")}</p>`;
                }
                filterButtons[a].classList.add('services--tabs--item--active');
            }
        })
    }



loadMoreBtn.addEventListener('click', function () {
    let a = 0;

    const arr = ['graphic-design', 'web-design', 'landing-page', 'wordpress'];
    const tabsArr = ['all', 'graphic-design', 'web-design', 'landing-page', 'wordpress'];

    for (let i = 1; i < 13; i++) {
        let lot = Math.floor(Math.random() * 4);


        let img = document.createElement("img");
        let overlay = document.createElement("div");
        let container = document.createElement("div");
        let iconContainer = document.createElement("div");
        let iconRef = document.createElement("div");
        let iconSearch = document.createElement("div");

        container.style.width = '284px'
        container.style.height = '206px'

        iconContainer.classList.add('icon--container');
        overlay.classList.add('after');
        container.classList.add('image-container');


        img.onerror = function () {
            this.style.display = 'none'
        };
        img.src = `img/${tabsArr[a]}/${tabsArr[a]}${i}.jpg`

        document.getElementById('our-work--list--cards').appendChild(container);
        container.appendChild(img);
        container.appendChild(overlay);

        let icon = document.createElement("img");
        icon.src = `img/icons/reference.png`;

        let icon2 = document.createElement("img");
        icon2.src = `img/icons/small-search.png`;

        iconRef.appendChild(icon);
        iconRef.classList.add('icon--style--ref');
        iconSearch.appendChild(icon2);
        iconSearch.classList.add('icon--style--search');
        iconContainer.appendChild(iconRef);
        iconContainer.appendChild(iconSearch);
        overlay.appendChild(iconContainer);

        overlay.innerHTML += "<h5 class='reviews--person--name'>CREATIVE DESIGN</h5>";

        if (a === 0) {

            img.src = `img/${arr[lot]}/${arr[lot]}${Math.floor((Math.random() * 6) + 1)}.jpg`
            overlay.innerHTML += `<p class='reviews--person--position'>${(arr[lot]).replace("-", " ")}</p>`;
        } else {

            overlay.innerHTML += `<p class='reviews--person--position'>${(tabsArr[a]).replace("-", " ")}</p>`;

        }

        loadMoreBtn.classList.add('hidden');
    }

})
























































