function createNewUser() {
    let firstName = prompt ('Please enter your name');
    let lastName = prompt ('Please enter your surname');
    let birthday = prompt ('Please enter your birthday dd.mm.yyyy');



    let newUser = { firstName: firstName,
        lastName: lastName,
        birthday: birthday,
        getLogin: function() {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase()
        },

        getAge: function () {
            let day = birthday.slice(0,2);
            let month = +birthday.slice(3,5)-1;
            let year = birthday.slice(6);
            let dateFormat = (year + ',' + month + ',' + day).toString();
            let dateEntered = new Date(dateFormat);

            return Math.floor((Date.now() - dateEntered) / 31536000000);
        },

        getPassword: function () {
           return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
        }

    };

    console.log(newUser.getLogin());
    console.log(newUser.getAge());
    console.log(newUser.getPassword());


    return newUser;

}


createNewUser();