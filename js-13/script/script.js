let switcher = document.getElementsByClassName('switcher');
let buttons = document.querySelectorAll(".btn");
let checker;
checker = localStorage.getItem('checker') || 'on' ;

boot ();

function boot () {
    if (checker === 'off' ) {
        addOptional();
    }

    else if (checker === 'on' )  {
        removeOptional();
    }
}


function handler () {
    if (checker === 'on') {
        on();
    }
   else if (checker === 'off')  {
        off();
    }
}

function addOptional() {
        buttons[0].classList.add('dark-btn-option');
        buttons[1].classList.add('green-btn-option');
        buttons[2].classList.add('red-btn-option');
        buttons[3].classList.add('swamp-btn-option');
}

function removeOptional(){
    buttons[0].classList.remove('dark-btn-option');
    buttons[1].classList.remove('green-btn-option');
    buttons[2].classList.remove('red-btn-option');
    buttons[3].classList.remove('swamp-btn-option');
}


function on() {
    addOptional();
    checker = 'off';
    localStorage.setItem('checker', checker);
}

function off() {
    removeOptional();
    checker = 'on';
    localStorage.setItem('checker', checker);

}

switcher[0].addEventListener('click', handler);
